package com.operations.arithmeticOperations;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class ArithmeticOperationTest {
    @Test
    void shouldCalculateSumWhenPositiveNumbersGive() {
        ArithmeticOperations sumOfPositiveNumbers = new ArithmeticOperations(4,5);
        int actualSumOfPositiveNumbers = sumOfPositiveNumbers.getSumOfNumbers();
        int expectedSumOfPositiveNumbers = 9;
        assertEquals(expectedSumOfPositiveNumbers, actualSumOfPositiveNumbers);
    }

    @Test
    void shouldCalculateSumWhenNegativeNumbersGive() {
        ArithmeticOperations sumOfNegativeNumbers = new ArithmeticOperations(-25,-5);
        int actualSumOfNegativeNumbers = sumOfNegativeNumbers.getSumOfNumbers();
        int expectedSumOfNegativeNumbers = -30;
        assertEquals(expectedSumOfNegativeNumbers, actualSumOfNegativeNumbers);
    }

    @Test
    void shouldCalculateSumWhenOnePositiveOneNegativeNumberGive() {
        ArithmeticOperations sumOfOnePositiveOneNegativeNumbers = new ArithmeticOperations(25,-5);
        int actualSumOfOnePositiveOneNegativeNumbers = sumOfOnePositiveOneNegativeNumbers.getSumOfNumbers();
        int expectedSumOfOnePositiveOneNegativeNumbers = 20;
        assertEquals(expectedSumOfOnePositiveOneNegativeNumbers, actualSumOfOnePositiveOneNegativeNumbers);
    }

    @Test
    void shouldCalculateDifferenceWhenPositiveNumbersGive() {
        ArithmeticOperations differenceOfPositiveNumbers = new ArithmeticOperations(30,5);
        int actualDifferenceOfPositiveNumbers = differenceOfPositiveNumbers.getDifferenceOfNumbers();
        int expectedDifferenceOfPositiveNumbers = 25;
        assertEquals(expectedDifferenceOfPositiveNumbers, actualDifferenceOfPositiveNumbers);
    }

    @Test
    void shouldCalculateDifferenceWhenNegativeNumbersGive() {
        ArithmeticOperations differenceOfNegativeNumbers = new ArithmeticOperations(30,5);
        int actualDifferenceOfNegativeNumbers = differenceOfNegativeNumbers.getDifferenceOfNumbers();
        int expectedDifferenceOfNegativeNumbers = 25;
        assertEquals(expectedDifferenceOfNegativeNumbers, actualDifferenceOfNegativeNumbers);
    }

    @Test
    void shouldCalculateDifferenceWhenOnePositiveOneNegativeNumberGive() {
        ArithmeticOperations differenceOfOnePositiveOneNegativeNumbers = new ArithmeticOperations(40,-5);
        int actualDifferenceOfOnePositiveOneNegativeNumbers = differenceOfOnePositiveOneNegativeNumbers.getDifferenceOfNumbers();
        int expectedDifferenceOfOnePositiveOneNegativeNumbers = 45;
        assertEquals(expectedDifferenceOfOnePositiveOneNegativeNumbers, actualDifferenceOfOnePositiveOneNegativeNumbers);
    }

    @Test
    void shouldCalculateMultiplicationWhenPositiveNumbersGive() {
        ArithmeticOperations multiplicationOfPositiveNumbers = new ArithmeticOperations(40,5);
        int actualMultiplicationOfPositiveNumbers = multiplicationOfPositiveNumbers.getMultiplicationOfNumbers();
        int expectedMultiplicationOfPositiveNumbers = 200;
        assertEquals(expectedMultiplicationOfPositiveNumbers, actualMultiplicationOfPositiveNumbers);
    }

    @Test
    void shouldCalculateMultiplicationWhenNegativeNumbersGive() {
        ArithmeticOperations multiplicationOfNegativeNumbers = new ArithmeticOperations(-12,-7);
        int actualDifferenceOfNegativeNumbers = multiplicationOfNegativeNumbers.getMultiplicationOfNumbers();
        int expectedDifferenceOfNegativeNumbers = 84;
        assertEquals(expectedDifferenceOfNegativeNumbers, actualDifferenceOfNegativeNumbers);
    }

    @Test
    void shouldCalculateMultiplicationWhenOnePositiveOneNegativeNumberGive() {
        ArithmeticOperations multiplicationOfOnePositiveOneNegativeNumbers = new ArithmeticOperations(-43,15);
        int actualMultiplicationOfOnePositiveOneNegativeNumbers = multiplicationOfOnePositiveOneNegativeNumbers.getMultiplicationOfNumbers();
        int expectedMultiplicationOfOnePositiveOneNegativeNumbers = -645;
        assertEquals(expectedMultiplicationOfOnePositiveOneNegativeNumbers, actualMultiplicationOfOnePositiveOneNegativeNumbers);
    }

    @Test
    void shouldCalculateDivisionWhenPositiveNumbersGive() {
        ArithmeticOperations divisionOfPositiveNumbers = new ArithmeticOperations(128,4);
        int actualDivisionOfPositiveNumbers = divisionOfPositiveNumbers.getDivisionOfNumbers();
        int expectedDivisionOfPositiveNumbers = 32;
        assertEquals(expectedDivisionOfPositiveNumbers, actualDivisionOfPositiveNumbers);
    }

    @Test
    void shouldCalculateDivisionWhenNegativeNumbersGive() {
        ArithmeticOperations divisionOfNegativeNumbers = new ArithmeticOperations(-414,-18);
        int actualDivisionOfNegativeNumbers = divisionOfNegativeNumbers.getDivisionOfNumbers();
        int expectedDivisionOfNegativeNumbers = 23;
        assertEquals(expectedDivisionOfNegativeNumbers, actualDivisionOfNegativeNumbers);
    }

    @Test
    void shouldCalculateDivisionWhenOnePositiveOneNegativeNumberGive() {
        ArithmeticOperations divisionOfOnePositiveOneNegativeNumbers = new ArithmeticOperations(525,-3);
        int actualDivisionOfOnePositiveOneNegativeNumbers = divisionOfOnePositiveOneNegativeNumbers.getDivisionOfNumbers();
        int expectedDivisionOfOnePositiveOneNegativeNumbers = -175;
        assertEquals(expectedDivisionOfOnePositiveOneNegativeNumbers, actualDivisionOfOnePositiveOneNegativeNumbers);
    }
}



