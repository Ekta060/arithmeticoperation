package com.operations.arithmeticOperations;

public class ArithmeticOperations {
    int firstNumber;
    int secondNumber;

    public ArithmeticOperations(int firstNumber,int secondNumber) {
        this.firstNumber = firstNumber;
        this.secondNumber=secondNumber;
    }

    public int getSumOfNumbers() {
        return this.firstNumber+this.secondNumber ;
    }

    public int getDifferenceOfNumbers() {
        return this.firstNumber-this.secondNumber ;
    }

    public int getMultiplicationOfNumbers() {
        return this.firstNumber*this.secondNumber ;
    }

    public int getDivisionOfNumbers() {
        return this.firstNumber/this.secondNumber ;
    }
}
